/*
 * Copyright (c) 2018 Jack D. de Boer.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wrapper;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

public class ApachiCliOptionsProvider {
    /**
     * This class implements the OptionsProvider interface by parsing the passed command line arguments.
     *
     * @author michiel
     */
    public static class ApacheCliOptionsProvider {
        private static final String HELP = "help";
        private static final String INSTANCES = "instances file";

        private final String[] clArguments;
        private Options options;
        private CommandLine commandLine;
        private String pathToInstances;

        /**
         * constructs with the command line array.
         *
         * @param args the CL array
         */
        public ApacheCliOptionsProvider(final String[] args) {
            this.clArguments = args;
            initialize();
        }

        /**
         * Options initialization and processing.
         */
        private void initialize() {
            buildOptions();
            processCommandLine();
        }

        /**
         * check if help was requested; if so, return true.
         * @return helpRequested
         */
        public boolean helpRequested() {
            return this.commandLine.hasOption(HELP);
        }


        /**
         * builds the Options object.
         */
        private void buildOptions() {
            // create Options object
            this.options = new Options();
            Option helpOption = new Option("h", HELP, false, "Prints this message");
            Option instancesOption = new Option("i", INSTANCES, true, "path to arff file containing instances to be classified by the model");
            options.addOption(helpOption);
            options.addOption(instancesOption);
        }

        /**
         * processes the command line arguments.
         */
        private void processCommandLine() {
            try {
                CommandLineParser parser = new DefaultParser();
                this.commandLine = parser.parse(this.options, this.clArguments);

                //check correct file format
                String filePath = this.commandLine.getOptionValue(INSTANCES);
                String extension = FilenameUtils.getExtension(filePath);
                if (extension.equals("arff")) {
                    this.pathToInstances = filePath;

                }else {
                    throw new IllegalArgumentException();

                }
            } catch (ParseException ex) {
                throw new IllegalArgumentException(ex);
            }
        }

        /**
         * prints help.
         */
        public void printHelp() {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("MyCoolTool", options);
        }

        public String getFilePath() {
            return this.pathToInstances;
        }
    }
}


