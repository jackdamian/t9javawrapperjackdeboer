/*
 * Copyright (c) 2018 Jack D. de Boer.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wrapper;

import java.util.Arrays;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI.
 **/
public class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */
    public void main(final String[] args) {
        try {
            ApachiCliOptionsProvider.ApacheCliOptionsProvider op = new ApachiCliOptionsProvider.ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            } else {
                ClassifierWrapper wrapper = new ClassifierWrapper();
                wrapper.main(op.getFilePath());
            }
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ApachiCliOptionsProvider.ApacheCliOptionsProvider op = new ApachiCliOptionsProvider.ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }
}
