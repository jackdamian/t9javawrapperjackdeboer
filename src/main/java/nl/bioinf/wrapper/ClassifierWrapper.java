/*
 * Copyright (c) 2018 Jack D. de Boer.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.wrapper;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.IOException;

public class ClassifierWrapper {
    private final String modelFile = "data/naivebayes.model";

    public static void main(String filePath) {
        ClassifierWrapper runner = new ClassifierWrapper();
        runner.start(filePath);
    }

    private void start(String filePath) {
        String datafile = "data/diabetic_data_modded_subset_4.arff";
        try {
            Instances instances = loadArff(datafile);
            printInstances(instances);
            NaiveBayes naiveBayes = buildClassifier(instances);
            saveClassifier(naiveBayes);
            NaiveBayes fromFile = loadClassifier();
            Instances unknownInstances = loadArff(filePath);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, unknownInstances);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(NaiveBayes bayes, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = bayes.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private NaiveBayes loadClassifier() throws Exception {
        // deserialize model
        return (NaiveBayes) weka.core.SerializationHelper.read(modelFile);
    }

    private void saveClassifier(NaiveBayes naiveBayes) throws Exception {
        weka.core.SerializationHelper.write(modelFile, naiveBayes);
    }

    private NaiveBayes buildClassifier(Instances instances) throws Exception {
        NaiveBayes bayes = new NaiveBayes();    // new instance of bayes
        bayes.buildClassifier(instances);       // build classifier
        return bayes;
    }

    private void printInstances(Instances instances) {
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}
